#require specific gems
require 'twitter'

#define class - remember, the class name should always start with a capital letter
class TwitterConfig
    #attribute reader
    attr_reader :twitterAccount
    #initialise class - used for configuring 
    def initialize (twitterAccount)
        @twitterAccount = twitterAccount
    end
    
    public
    def printName
        #confirming the name of the twitter account set to
        puts "Account set to - #{twitterAccount}"
    end

    public
    def viewTweets
        puts "How many tweets from the user - #{twitterAccount} - do you want to view?"
        number = gets.chomp
        #configuring twitter config
        client = Twitter::REST::Client.new do |config|
        config.consumer_key = ''
        config.consumer_secret = ''
        end
        puts "\nLet's take a look at some recent tweets from a user!"
        #uses the user_timeline method to find the account, count is returning the number of tweets
        tweets = client.user_timeline(twitterAccount, count: number)
        tweets.each { |tweet| puts tweet.full_text }
    end

    public
    def phraseTweets
        puts "Please enter the phrase you want to see tweets for from users!"
        phrase = gets.chomp
        puts "How many tweets do you want to see?"
        num = gets.chomp.to_i
        #configuring twitter config
        client = Twitter::REST::Client.new do |config|
        config.consumer_key = ''
        config.consumer_secret = ''
        end
        #searching tweets from a user with a specific phrase most recent displayed
        #for each tweet it finds, display it to the screen
        #count is returning the number of tweets per page - max is 100
        #result type specifies what type of search results you want
        #the take method returns specified number of elements (tweets in this case) from an array
        #And finally, there is an each method to print every tweet it finds
        client.search("to:#{twitterAccount} #{phrase}", :count => 1, :result_type => "recent").take(num).each do |tweet|
        puts tweet.text
        end
    end

    public
    def streamTweets
        #configuring a new streaming client with relevant keys
        client = Twitter::Streaming::Client.new do |config|
        config.consumer_key        = ''
        config.consumer_secret     = ''
        config.access_token        = ''
        config.access_token_secret = ''
        end
        puts "Please enter a topic you wish to stream tweets about. Please note that this will stream tweets in real time!"
        #get user choice for topic
        topic = gets.chomp
        #for every tweet it finds, print to the screen
        #pass the topic variable to the track options hash (part of the filter method)
        client.filter(track: topic) do |object|
        #prints the tweet if the object is a tweet (from the Twitter class)
        puts object.text if object.is_a?(Twitter::Tweet)
        end
    end
end

puts "Welcome to Twitter on Ruby!"
puts "Please state the twitter user name you wish to view tweets for. Make sure that the account name is correct!"
userName = gets.chomp
#creates a new instance of the TwitterConfig class
twitterSet = TwitterConfig.new(userName)
#calls the method to print the username the user chose
twitterSet.printName

#boolean declared - set to true
check = true

#while loop to ensure that the program loops.
while check == true
    puts "Please pick a menu choice by typing the letter in brackets!"
    puts "(V)iew tweets from a user"
    puts "(F)ind tweets with a specific phrase"
    puts "(S)tream Tweets"
    puts "(Q)uit"
    #get user choice and convert it to upper case
    userChoice = gets.chomp.upcase
    case userChoice
    when 'V'
        twitterSet.viewTweets
    when 'F'
        twitterSet.phraseTweets
    when 'S'
        twitterSet.streamTweets
    when 'Q'
        puts "Quit"
        #sets the boolean value to false and exits the program
        check = false
    #if a user enters a command that isn't valid - run this!
    else
        puts "Invalid command - try again!"
    end
end